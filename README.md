# The Socially Project
 Socially is an open source project and its code is available to everyone.

# UI
The UI is self designed and is user friendly the design is mobile responsive and fast.

# Type of authentication
1. Email,Password

# Objective of Socially
In today's world there are a lot of problems that can happen around us or can even happen to us but we fail to report them.Through our service you can report your problems and stay anonymous(optional) Whether the problem is about 
   1. Neibourhood
   2. Bullying
   3. or other muncipality problems

We have got you covered.

# Libraries and services used 
1. [Chart.js](https://www.chartjs.org/)
3. [Animate.css](https://animate.style/)
4. [Bootstrap5](https://getbootstrap.com/docs/5.0/getting-started/introduction/)
5. [fontawesome](https://fontawesome.com)
6. [jquery](https://jquery.com/)
7. [sweetalert2@10](https://sweetalert2.github.io/)
8. [Firebase](https://console.firebase.google.com/)

# Maintenance and upcoming updates
the admin dashboard is yet to come